import { Container, Row } from "react-bootstrap";
import RenderDataUser from "../components/RenderDataUser";
import ModalUser from "../components/ModalUser";
import { useState } from "react";

const UserPage = () => {
    let [list, setList] = useState([]);

    function addNewList(payload) {
        console.log("MAIN", payload);
        setList((prevsValue) => {
            return [...prevsValue, payload];
        });
    }

    const [show, setShow] = useState(false);
    const handleClose = () => setShow(false);
    const handleShow = () => setShow(true);

    const [message, setMessage] = useState("");
    const customMessage = (payload) => {
        // console.log(payload);
        setMessage(payload);
    };

    return (
        <Container fluid="md pt-5">
            <Row>
                <RenderDataUser
                    dataUser={list}
                    addNew={addNewList}
                    setHandleShow={handleShow}
                    message={customMessage}
                />
                <ModalUser show={show} close={handleClose} message={message} />
            </Row>
        </Container>
    );
};
export default UserPage;
