import { Form } from "react-bootstrap";
import { useState } from "react";
import ButtonAddUser from "../components/ButtonAddUser";

export default function FormUser(props) {
    let [listUser, setListUser] = useState({
        username: "",
        age: 0,
    });
    function usernameHandler(events) {
        setListUser((prevsValue) => {
            return {
                ...prevsValue,
                username: events.target.value,
            };
        });
    }
    function ageHandler(events) {
        setListUser((prevsValue) => {
            return {
                ...prevsValue,
                age: events.target.value,
            };
        });
    }

    function newDataHandler(events) {
        if (listUser.username === "" || listUser.age === "") {
            events.preventDefault();
            props.message("username or age cannot be empty!");
            props.handleShow();
            return;
        } else if (Math.sign(listUser.age) <= 0) {
            events.preventDefault();
            props.message("Invalid age <1");
            props.handleShow();
            return;
        } else {
            events.preventDefault(); // biar engga refresh after submit button
            props.addUser(listUser);
            props.message("Add user successfully");
            props.handleShow();
            return;
        }
    }

    return (
        <div>
            <h5 className="text-center">Add Data User</h5>
            <Form onSubmit={newDataHandler} className="p-1 ">
                <Form.Group className="mb-3" controlId="formBasicEmail">
                    <Form.Label>Username</Form.Label>
                    <Form.Control
                        type="text"
                        placeholder="Enter Username"
                        onChange={usernameHandler}
                    />
                    <Form.Text className="text-muted"></Form.Text>
                </Form.Group>
                <Form.Group className="mb-3" controlId="formBasicPassword">
                    <Form.Label>Age (Years)</Form.Label>
                    <Form.Control
                        type="number"
                        placeholder="Enter Age "
                        onChange={ageHandler}
                    />
                </Form.Group>
                <ButtonAddUser show={newDataHandler} />
            </Form>
        </div>
    );
}
