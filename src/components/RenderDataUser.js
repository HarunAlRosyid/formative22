import { Col, Table } from "react-bootstrap";
import FormUser from "./FormUser";

export default function RenderDataUser(props) {
    return (
        <div>
            <Col
                lg={10}
                className="p-5 m-auto shadow-lg rounded bg-white d-grid gap-2"
            >
                <FormUser
                    addUser={props.addNew}
                    handleShow={props.setHandleShow}
                    message={props.message}
                />
            </Col>

            <Col
                lg={10}
                className="mt-5 mb-5 p-5 m-auto shadow-lg rounded bg-white d-grid gap-2"
            >
                <Table striped bordered hover size="sm">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Username</th>
                            <th>Age (Years)</th>
                        </tr>
                    </thead>
                    <tbody>
                        {props.dataUser.length > 0 ? (
                            props.dataUser.map((value, index) => {
                                return (
                                    <tr key={index}>
                                        <td>{index + 1}</td>
                                        <td>{value.username}</td>
                                        <td>{value.age}</td>
                                    </tr>
                                );
                            })
                        ) : (
                            <td className="text-center" colSpan="3">
                                No Data
                            </td>
                        )}
                    </tbody>
                </Table>
            </Col>
        </div>
    );
}
