import { Button } from "react-bootstrap";

export default function ButtonAddUser(props) {
    return (
        <div>
            <Button
                size="large"
                variant="primary"
                type="submit"
                onClick={props.show}
            >
                Add User
            </Button>
        </div>
    );
}
