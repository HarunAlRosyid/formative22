import "./App.css";

import UserPage from "./screens/UserPage";

function App() {
    return (
        <div className="bg-primary">
            <UserPage />
        </div>
    );
}

export default App;
